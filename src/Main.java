import java.util.Scanner;

public class Main {

    private static PeselValidator Pesel;

    public static void main(String[] args) {

        System.out.println("Insert PESEL number:");
        Scanner sc = new Scanner(System.in);
        String PESEL = sc.nextLine();
        Pesel = new PeselValidator(PESEL);

        if (Pesel.isValid()) {
            System.out.println("PESEL is correct");
            System.out.println("Sex: " + Pesel.getSex());
            System.out.println("Day of birth: " + Pesel.getBirthDay() + " - " + Pesel.getDayName());
            System.out.println("Month of birth: " + Pesel.getBirthMonth() + " - " + Pesel.getMonthName());
            System.out.println("Year of birth: " + Pesel.getBirthYear());
            System.out.println("Birth date: " + Pesel.getBirthDate());
        }
        else {
            System.out.println("PESEL is invalid");
        }
    }
}
