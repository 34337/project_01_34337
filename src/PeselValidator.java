import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class PeselValidator {


    private final int[] PESEL = new int[11];
    private boolean valid = false;
    public String dayName;
    private static final int[] weightTable = {9, 7, 3, 1, 9, 7, 3, 1, 9, 7};

    public PeselValidator(String PESELNumber) {
        if (PESELNumber.length() != 11){
            valid = false;
        }
        else {
            for (int i = 0; i < 11; i++){
                PESEL[i] = Byte.parseByte(PESELNumber.substring(i, i+1));
            }
            if (checkSum() && checkMonth() && checkDay()) {
                valid = true;
            }
            else {
                valid = false;
            }
        }
    }

    public boolean isValid() {
        return valid;
    }

    public int getBirthYear() {
        int year;
        int month;
        year = 10 * PESEL[0];
        year += PESEL[1];
        month = 10 * PESEL[2];
        month += PESEL[3];
        if (month > 80 && month < 93) {
            year += 1800;
        }
        else if (month > 0 && month < 13) {
            year += 1900;
        }
        else if (month > 20 && month < 33) {
            year += 2000;
        }
        else if (month > 40 && month < 53) {
            year += 2100;
        }
        else if (month > 60 && month < 73) {
            year += 2200;
        }
        return year;
    }

    public int getBirthMonth() {
        int month;
        month = 10 * PESEL[2];
        month += PESEL[3];
        if (month > 80 && month < 93) {
            month -= 80;
        }
        else if (month > 20 && month < 33) {
            month -= 20;
        }
        else if (month > 40 && month < 53) {
            month -= 40;
        }
        else if (month > 60 && month < 73) {
            month -= 60;
        }
        return month;
    }

    public String getMonthName(){
        Locale locale = Locale.ENGLISH;
        int month = getBirthMonth();
        return new DateFormatSymbols(locale).getMonths()[month - 1];
    }

    public int getBirthDay() {
        int day;
        day = 10 * PESEL[4];
        day += PESEL[5];
        return day;
    }

    public String getDayName() {
        int year = getBirthYear();
        int month = getBirthMonth();
        int day = getBirthDay();

        int[] t = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
        String[] dayNames = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        if (month < 3) {
            year--;
        }
        int dayNumber = (int) (year + Math.floor(year / 4) - Math.floor(year / 100) + Math.floor(year / 400) + t[month - 1] + day) % 7;
        return dayName = dayNames[dayNumber];
    }

    public String getBirthDate(){
        int year = getBirthYear();
        int month = getBirthMonth();
        int day = getBirthDay();

        String date = LocalDate.of(year,month,day).format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        return date;
    }

    public String getSex() {
        if (valid) {
            if (PESEL[9] % 2 == 1) {
                return "Male";
            }
            else {
                return "Female";
            }
        }
        else {
            return "";
        }
    }

    private boolean checkSum() {
        int sum = 0;
        for (int i = 0; i < 10; i++){
            sum += (PESEL[i] * weightTable[i]);
        }
        sum %= 10;

        if (sum == PESEL[10]) {
            return true;
        }
        else {
            return false;
        }
    }
    
    private boolean checkMonth() {
        int month = getBirthMonth();
        if (month > 0 && month < 13) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean checkDay() {
        int year = getBirthYear();
        int month = getBirthMonth();
        int day = getBirthDay();
        if ((day > 0 && day < 32) &&
                (month == 1 || month == 3 || month == 5 ||
                        month == 7 || month == 8 || month == 10 ||
                        month == 12)) {
            return true;
        }
        else if ((day > 0 && day < 31) &&
                (month == 4 || month == 6 || month == 9 ||
                        month == 11)) {
            return true;
        }
        else if ((day > 0 && day < 30 && leapYear(year)) ||
                (day > 0 && day < 29 && !leapYear(year))) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean leapYear(int year) {
        if (year % 4 == 0)
            return true;
        else
            return false;
    }
}
