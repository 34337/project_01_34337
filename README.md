# Walidator PESEL

| Wajdowicz Sylwester | Warzecha Daniel |
| ------ | ------ |

Aplikacja umożliwia walidację numeru PESEL. Podaje informacje na temat daty urodzenia oraz płci na podstawie podanego numeru PESEL.
Podczas uruchomienia aplikacji, użytkownik proszony jest o wpisanie swojego numeru PESEL, po zatwierdzeniu, program zwraca informacje oparte na podanym numerze. Użytkownik może sprawdzić, czy podany numer jest poprawny, jeżeli tak, otrzymuje informacje o płci, dniu, miesiącu i roku urodzenia oraz pełną datę urodzenia w formacie dd-mm-yyyy.

Przykłady zastosowania:


![1](https://i.imgur.com/QJHgPda.png)
![2](https://i.imgur.com/Ap5YX0g.png)



Do utworzenia, debugowania i testowania apliakcji korzystaliśmy ze środowiska IntelliJ.
Wykorzystany został system kontroli wersji GIT oraz serwis wykorzystujący system GIT -  BitBucket.


Podział prac:


| Wajdowicz Sylwester | Warzecha Daniel |
| ------ | ------ |
| Metody dotyczące pobierania roku i dnia z numeru PESEL | Klasa PeselWalidator |
| Metoda zwracająca nazwę dnia urodzin | Metoda pobierająca numer miesiąca z PESEL-u |
| Metoda sprawdzająca sumę kontrolną | Metoda wyznaczająca nazwę miesiąca |
| Metoda sprawdzająca poprawność miesiąca | Metoda wyznaczjąca pełną datę urodzenia |
| Metoda sprawdzająca poprawność miesiąca | Metoda wyznaczjąca płeć |
| Wywołanie metod w Main | Metoda sprawdzająca ilość dni w miesiącu |
| | Wywołanie metod w Main |


